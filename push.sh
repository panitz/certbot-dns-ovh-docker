#!/bin/bash

TARGET_ARCH=${TARGET_ARCH:=amd64}
CERTBOT_VERSION=${CERTBOT_VERSION:=1.4.0}
DOCKER_REPO=${DOCKER_REPO:=panitz/certbot-dns-ovh}
DOCKER_TAG=${TARGET_ARCH}-v${CERTBOT_VERSION}

docker push ${DOCKER_REPO}:${DOCKER_TAG}

echo "Do you also wish to push the image as latest?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) docker tag ${DOCKER_REPO}:${DOCKER_TAG} ${DOCKER_REPO}:latest && docker push ${DOCKER_REPO}:latest; break;;
        No ) exit;;
    esac
done