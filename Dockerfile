# Docker Arch (amd64, arm32v6, ...)
ARG TARGET_ARCH=amd64
ARG CERTBOT_VERSION=1.4.0
FROM certbot/dns-ovh:${TARGET_ARCH}-v${CERTBOT_VERSION}

RUN apk add --no-cache curl ssmtp
