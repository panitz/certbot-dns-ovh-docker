# Dockerfile with certbot, dns-ovh plugin and extra tools

This dockerfile builds a certbot image with the dns-ovh plugin. It also contains the following tools:

- curl
- ssmtp

## Build docker image

```shell
$ ./build.sh
```

The build script accepts the following arguments:

| Argument | Default value | Description |
|-|-|-|
| TARGET_ARCH | amd64 | Target architecture. For valid architecures see [certbot/dns-ovh](https://hub.docker.com/r/certbot/dns-ovh/tags). |
| CERTBOT_VERSION | 1.4.0 | Version of certbot. For valid version see [certbot/dns-ovh](https://hub.docker.com/r/certbot/dns-ovh/tags). |
| DOCKER_REPO | panitz/certbot-dns-ovh | Repository of the docker image. |

### Examples

```shell
$ TARGET_ARCH=arm64v8 ./build.sh
```

```shell
$ CERTBOT_VERSION=1.3.0 ./build.sh
```
